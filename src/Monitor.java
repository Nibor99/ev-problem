/**
 * Created by rbeer on 14.12.2016.
 */
public class Monitor {
    public static void main(String[] args) {
        Puffer p = new Puffer();
        Erzeuger e = new Erzeuger(p);
        Verbraucher v = new Verbraucher(p);
        e.start();
        v.start();
    }
}
