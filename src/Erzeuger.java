/**
 * Created by rbeer on 14.12.2016.
 */
public class Erzeuger extends Thread{
    private Puffer puffer;
    int i = 0;

    public Erzeuger(Puffer puffer) {
        this.puffer = puffer;
    }

    @Override
    public void run() {
        while(i<10){
            puffer.put(i);
            System.out.println("Erzeuger put " + i);
            i++;
            try {
                sleep((int)Math.random()*1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
