import java.util.Objects;

public class Puffer {
    private Object[] puffer;
    private int erstesElement, naechstesElement, anzahl;

    public Puffer() {
        anzahl = 0;
        erstesElement = 0;
        naechstesElement = 0;
        puffer = new Object[10]; //Puffer hat kapazitaet 10
    }

    public synchronized Object get(){
        while(true){
            try {
                if(anzahl == 0){
                    wait();
                }
                else break;
            }
            catch (InterruptedException e){}
        }
        Object ob = puffer[erstesElement];
        puffer[erstesElement] = null;
        anzahl--;
        erstesElement = (erstesElement+1)%puffer.length;
        notify();
        return  ob;
    }

    public synchronized void put(Object element){
        while(true){
            try {
                if(anzahl >= puffer.length){
                    wait();
                }
                else {
                    break;
                }

            }
            catch (InterruptedException e){}

        }
        puffer[naechstesElement] = element;
        naechstesElement = (naechstesElement+1)%puffer.length;
        anzahl++;
        notify();
    }

    public static void main(String[] args) {
        Puffer p = new Puffer();
        Erzeuger e = new Erzeuger(p);
        Verbraucher v = new Verbraucher(p);
        e.start();
        v.start();

    }


}
